from .base import *
import os


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('DB_NAME'),
        'PASSWORD': os.environ.get('DB_PASSWORD'),
        'USER': os.environ.get('DB_USER'),
        'HOST': '',
        'PORT': '',
        'CONN_MAX_AGE': None
    }
}

# DEBUG_TOOLBAR_PATCH_SETTINGS = False

# MIDDLEWARE_CLASSES += ['debug_toolbar.middleware.DebugToolbarMiddleware']

INSTALLED_APPS += ['debug_toolbar']
# INTERNAL_IPS = ['10.0.2.2']  # Adjust this to request.META['REMOTE_ADDR']

DEBUG = True

EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'

EMAIL_FILE_PATH = '/var/log/wall/emails'
