env=development
process=gunicorn
group=all
app=core
tags=all

ANSIBLE = ansible $(group) -i devops/inventory/$(env)
ANSIBLE_PLAYBOOK = ansible-playbook -i devops/inventory/$(env)

# Match any playbook in the devops directory
% :: devops/%.yml 
	$(ANSIBLE_PLAYBOOK) $< -l $(group) --tags $(tags)
	
staging :
	$(eval env = staging )
	$(ANSIBLE_PLAYBOOK) devops/deploy.yml

status :
	$(ANSIBLE) -s -a "sudo monit status" 

restart-all :
	$(ANSIBLE) -s -a "monit restart all" 

restart-process:
	$(ANSIBLE) -m monit -a "name=$(process) state=restarted" --sudo

restart-monit :
	ansible app_servers -i devops/inventory/$(env) -m shell -s \
	-a "service monit restart"

test:
	$(ANSIBLE) -m django_manage -a "command=test app_path=/vagrant virtualenv=/home/vagrant/.virtualenvs/wall apps='core'"
	$(ANSIBLE) -m django_manage -a "command=test app_path=/vagrant virtualenv=/home/vagrant/.virtualenvs/wall settings=wall.settings.allauth_test apps='allauth.socialaccount allauth.account'"
	
makemigrations:
	$(ANSIBLE) -m django_manage -a "command='makemigrations $(app)' app_path=/vagrant virtualenv=/home/vagrant/.virtualenvs/wall"

migrate:
	$(ANSIBLE) -m django_manage -a "command='migrate $(app)' app_path=/vagrant virtualenv=/home/vagrant/.virtualenvs/wall"
