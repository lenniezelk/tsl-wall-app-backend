from django.contrib.sites.models import Site
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Create site with domain localost:5000'

    def handle(self, *args, **options):
        Site.objects.get_or_create(domain='localhost:5000')        
