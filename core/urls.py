from django.conf.urls import include, url
from rest_framework import routers

from .views import (
    CurrentUserView, ConfirmationView, PostViewSet, UserCreateView, UserViewSet)


router = routers.SimpleRouter()
router.register(r'^posts', PostViewSet, base_name='post')
router.register(r'^users', UserViewSet)

urlpatterns = router.urls

urlpatterns += [
    url(r'^confirm-email/(?P<key>\w+)/?$', ConfirmationView.as_view(),
        name='confirm-email'),
    url(r'^users/create/?$', UserCreateView.as_view(), name='user-create'),
    url(r'^users/current/?$', CurrentUserView.as_view(), name='user-current'),
]
