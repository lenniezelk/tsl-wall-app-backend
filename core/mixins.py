from rest_framework import mixins, viewsets


class ListRetrieveUpdateDestroyMixin(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    mixins.ListModelMixin,
    ):
    """
    Allow list, retrieve, update and destroy operations
    """
    pass


class ListRetrieveUpdateDestroyViewSet(ListRetrieveUpdateDestroyMixin, viewsets.GenericViewSet):
    """
    Allow list, retrieve, update and destroy operations
    """
    pass


class RetrieveGenericViewSet(
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet
    ):
    """
    Allow retrieve operation only
    """
    pass
