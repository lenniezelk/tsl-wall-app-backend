import uuid

from allauth.account.models import EmailAddress, EmailConfirmation
from allauth.utils import generate_unique_username
from annoying.functions import get_object_or_None
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.utils import timezone
from rest_framework import exceptions, permissions, status, viewsets
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from .mixins import ListRetrieveUpdateDestroyViewSet
from .models import Post, User
from .serializers import PostSerializer, UserSerializer
from .utils import random_string


class CurrentUserView(APIView):
    """
    Current User View
    """
    def post(self, request):
        if request.user.is_authenticated():
            return Response(
                UserSerializer(request.user).data)
        return Response(
            status=status.HTTP_404_NOT_FOUND
        )


class UserCreateView(APIView):
    """
    User Create View
    """
    permission_classes = tuple()
    authentication_classes = tuple()

    def post(self, request):
        """
        Override to create user and set password in one fell swoop
        """
        email = request.data.get('email')
        password1 = request.data.get('password1')
        password2 = request.data.get('password2')
        first_name = request.data.get('first_name')
        last_name = request.data.get('last_name')

        # check required fields are provided
        if not email or not password1 or not password2:
            raise exceptions.ValidationError(
                    {'email': 'required',
                    'password1': 'required',
                    'password2': 'required'}
            )

        if password1 != password2:
            raise exceptions.ValidationError(
                {'password2': 'must match password1'}
            )

        email_address = get_object_or_None(EmailAddress, email=email)

        if email_address:
            raise exceptions.ValidationError(
                {'email': 'User with this email already exists'}
            )

        user = User.objects.create_user(
            username=generate_unique_username(email), email=email,
            password=password1, first_name=first_name, last_name=last_name)

        email_address = EmailAddress.objects.create(user=user, email=email)

        Token.objects.create(user=user)

        # send email confiramtion
        confirmation = EmailConfirmation.objects.create(
            key=random_string(),
            email_address=email_address)

        url = reverse(
            "confirm-email",
            kwargs={'key': confirmation.key})

        activation_url = settings.FRONTEND_URL + url

        template_values = {
            'user': user,
            'activation_url': activation_url
        }

        body = render_to_string('email/email_confirmation.txt', template_values)

        email_message = EmailMessage(
            subject='Confirm your email - Wall App',
            body=body,
            to=(email_address.email,)
            )

        email_message.send()

        confirmation.sent = timezone.now()

        confirmation.save()

        response_data = {
            'user': UserSerializer(user).data,
            'token': user.auth_token.key
        }

        return Response(
            response_data
        )


class ConfirmationView(APIView):
    """
    Email Confirmation View
    """
    permission_classes = tuple()
    authentication_classes = tuple()

    def get(self, request, key=None):
        confirmation = get_object_or_404(EmailConfirmation, key=key)

        confirmation.email_address.verified = True

        confirmation.save()

        return Response()


class PostViewSet(viewsets.ModelViewSet):
    """
    Post View Set
    """
    serializer_class = PostSerializer
    queryset = Post.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, )

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class UserViewSet(ListRetrieveUpdateDestroyViewSet):
    """
    User View Set
    """
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def get_object(self):
        return self.request.user
