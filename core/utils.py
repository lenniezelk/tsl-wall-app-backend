import binascii
import os

def random_string(n=5):
    """
    Generate a random string
    """
    random_bytes = os.urandom(n)
    return binascii.hexlify(random_bytes).decode()
