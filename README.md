# Wall App Back end #

### What is this repository for? ###

* https://docs.google.com/document/d/1mCsQUHatIj7gEcGNq6Q3FurGotUHmu-HLtnc9Fe_vBc/edit#
* Wall App back end

### How do I get set up? ###

* Ensure `virtualenv` and `virtualenvwrapper` are installed: https://virtualenvwrapper.readthedocs.io/en/latest/install.html
* Run `mkvirtualenv tsl-wall-app-backend`
* Clone this repository and `cd` into the project directory.
* Run `pip install -r requirements/development.txt`
* Run `python manage.py migrate`
* Run `python manage.py create_site`
* Run `python manage.py runserver 5000`