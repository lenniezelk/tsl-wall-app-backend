from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render
from rest_framework import exceptions, status, viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from core.serializers import UserSerializer


# Create your views here.
class LogoutView(APIView):
    """
    Logout View
    """
    permission_classes = tuple()
    authentication_classes = tuple()

    def post(self, request):

        logout(request)

        return Response()


class LoginView(APIView):
    """
    Login View
    """
    permission_classes = tuple()
    authentication_classes = tuple()

    def post(self, request):
        email = request.data.get('email')
        password = request.data.get('password')

        if not email or not password:
            return Response(
                {'email': 'required',
                'password': 'required'
                },
                status=status.HTTP_400_BAD_REQUEST)

        user = authenticate(email=email, password=password)

        if not user:
            return Response(
                status=status.HTTP_401_UNAUTHORIZED
            )

        response_data = {
            'user': UserSerializer(user).data,
            'token': user.auth_token.key
        }

        return Response(
            response_data
        )
